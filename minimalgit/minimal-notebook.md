---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.9.1
  kernelspec:
    display_name: my-env
    language: python
    name: my-env
---

### Minimal Notebook

This notebook serves as a basis for testing various notebook versioning approaches. 

For example:
  * nbdime
  * jupytext

```python
from functools import reduce
import pandas as pd

l = [1,2,3]
reduce(lambda x, y: x+y, l)
```

```python
%matplotlib inline


df = pd.DataFrame([[0,1,2,3,4,5], [1,1,2,2,1,0]])
df.T.set_index(0).plot()
```

```python

```
