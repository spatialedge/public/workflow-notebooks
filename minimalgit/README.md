# Versioning your notebooks


Notebooks may be unweilding in git commits, so what we do is convert them to .md file and review those.


### Version control

We will consider _jupytext_ and _nbdime_ in this folder as a means manage version control on jupyter notebooks in a responsible manner.

It is likely best to use both as we will still want to commit ipynb files for the sake of rendering them pleasantly in the repository manager (Github or Gitlab).

In this case _jupytext_ is the more critical of the two approaches, so make sure that it works.

**tl;dr**

   > We will commit two files for each notebook, the original _ipynb_ and a second markdown file that jupytext will generate for us. We can then do merge requests on the markdown.


#### Jupytext


JupyText converts notebooks to markdown and maintains a notion of a notebook _pair_, thus being able to reconstitute the original `ipynb`.

##### Installation

```
pip install jupytext --upgrade
```

With it installed, it is now a simple matter to generate the markdown version of your notebook before committing.

```
jupytext --to md minimal-notebook.ipynb
```

This will result in the markdown file `minimal-notebook.md`. Now you can add, stage and commit it with the notebook.


If you want to use it in jupyterlab by selecting menu items, you may install and enable the extension (it will then manage both copies for you). The instructions are (see [[2]]) to enable this:

```
jupyter nbextension install --py jupytext [--user]
jupyter nbextension enable --py jupytext [--user]
```

#### NBDime

NBDime performs "content aware" difference on notebooks, and is very useful when running a `git diff` on your local working copy before pushing it. There is also a nice difference tool that it an open in your browser.

##### Installation

```
pip install nbdime
```
After installing it, integrate it with git:
```
nbdime config-git --enable --global
```


### Example

Consider this notebook:

![](before-notebook.png)

We modify it to the following:

![](after-notebook.png)

With NBDime installed, we see the following upon execution of `git diff`:

![](nbdime.png){width=60%}



Interestingly, if run the `--to md` line from `jupytext`, nothing changes in the markdown as it automatically strips the output.

If we were to change the Dataframe values, then it would change the markdown too. If you convert the markdown back to a notebook, then you lose your output sadly.

You can do this by running:

```
jupytext --to notebook minimal-notebook.md
```


[1]: https://jupytext.readthedocs.io/en/latest/using-cli.html
[2]: https://jupytext.readthedocs.io/en/latest/install.html
[3]: https://nbdime.readthedocs.io/en/latest/
