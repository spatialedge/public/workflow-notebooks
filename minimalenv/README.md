## Environment Isolation

To collaborate effectively and to make it possible for others to reproduce our work, it is important that we isolate the environment adequately. 

To this end, we will use _virtual environments_ in python to isolate the packages we use.


### Create a virtualenv


Firstly, you will need to ensure that you have the requisite software/packages installed. You will need python's package manager (`pip`).


If you are in a debian based linux system, run: 

```bash
sudo apt install python3-pip
```

You may now install the virtual environment package (the `--user` flag is important to set your paths up correctly) 

```bash
pip install --user virtualenv
```

Now, create a virtual environment by running

```bash
python3 -m virtualenv ~/my-virtualenv/my-env
```

A virtual environment called my-env has now been created for you. You can activate it using the command:

```bash
source ~/my-virtualenv/my-env/bin/activate
```

### Managing your new environment

Now that you have activated the environment, you should see a prompt like this:

```bash
(my-env) ubuntu@play:~$ 
```

We can list all applications in that environment using pip. For example, to list packages currently installed in this environment:

```bash
pip freeze
```

To install a package we need (e.g. `pandas`), run:

```bash
pip install pandas
```

The way you leverage `pip` here is exactly how you might do it in a hosted environment like [colab](https://colab.research.google.com). In this case we are interested in 
managing these packages to make our code portable (an environment like _colab_ or _kaggle notebooks_ isolates the environment by virtue of it being a single hosted instance - you may want to create independent
environments if you have multiple applications in a single _colab_ instance).

If you list your installed packages now, you will notice that `pandas` has been installed:


```bash
(my-env) ubuntu@play:~$ pip freeze

numpy==1.21.2
pandas==1.3.3
python-dateutil==2.8.2
pytz==2021.3
six==1.16.0
```


### Capturing your dependencies

To make your application portable and shareable, you will need to capture the packages in your environment. You may do this in a unix-like prompt with the command:

```bash
pip freeze > requirements.txt
```

This command redirects the output of the `pip freeze` command to a file called `requirements.txt`.


Collaborators may now use this file to create an environment in which your code can run. 
They would do this by accessing the file (which you have hopefully committed with your code) and installing the dependencies in their environment (hopefully a virtual environment too).

```bash
python3 -m virtualenv ~/my-virtualenv/other-env
source ~/my-virtualenv/other-env/bin/activate
pip install -r requirements.txt
```



