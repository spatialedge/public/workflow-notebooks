# Notebooks workflow


An example of how to approach collaborative and reproducible data science using widely available tools, namely, 

  * Git
  * JupyterLab notebooks

and 

  * Patterns & practices.
  
  
This is a simple first order approach that does not require you to adopt more opinionated tooling (e.g. Kedro) and doesn't store too data in the repository itself (e.g. DVC). 

In summary, this is a first-principles approach which will allow you to consider some of those tools later in your approach. 


## Contents

This repository contains: 

  * Some basic git-fu for working with notebooks (`minitmalgit/`)
  * An exploratory data analysis notebook example that we make more responsible (`pinciples`)
  
  
  
